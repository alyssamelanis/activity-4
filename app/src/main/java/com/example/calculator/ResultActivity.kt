package com.example.calculator

import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class ResultActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result)
        val result = intent.getStringExtra("RESULT")

        val textView = findViewById<TextView>(R.id.txtResult).apply {
            val result = "HASIL $result"
            text = result
        }
    }
}